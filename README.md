# Check By Jdbc Nagios Plugin #

This is the original official CheckByJdbc nagios plugin, developed by josemarsilva@yahoo.com.br 

I modified to return UNKNOWN status when there is no data.


```
#!java
if( numberOfRows == 0)
{
    retStatus = 3;
    nCountStatusUNKNOWN = 1;
}
```

Please, refer to the link below for detailed information about this plugin.
 
http://exchange.nagios.org/directory/Plugins/Databases/CheckByJdbc/details
