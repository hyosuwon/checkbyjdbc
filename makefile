#! /bin/sh
echo off
echo
echo Compiling ...
echo - CheckByJdbc.java
echo

javac -cp ./bin/:.:./lib/classes12.jar:./lib/JSAP-2.0a.jar ./bin/CheckByJdbc.java


echo
echo Running ...
echo
echo - java -cp . CheckByJdbc -h
echo - See some samples of use in 'Samples.bat' or 'Samples.sh' ...
echo
